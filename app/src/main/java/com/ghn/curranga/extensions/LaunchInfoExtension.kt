package com.ghn.curranga.extensions

import com.ghn.curranga.data.model.response.LaunchItem
import com.ghn.curranga.data.model.response.LaunchpadResponse
import com.ghn.curranga.data.model.response.RocketResponseItem
import com.ghn.curranga.ui.home.LaunchInfo
import com.ghn.curranga.util.DateUtils

fun LaunchItem.toLaunchInfo(
    pad: LaunchpadResponse?,
    rocketResponseItem: RocketResponseItem?
): LaunchInfo {
    return LaunchInfo(
        id = id,
        name = name,
        status = success,
        date = DateUtils.getFormattedDate(dateUtc),
        details = details,
        images = links?.flickr?.original,
        patchIcon = links?.patch?.small,
        youtubeId = links?.youtubeId,
        rocketName = rocketResponseItem?.name,
        launchpadName = pad?.fullName,
        launchpadDetails = pad?.details,
        launchpadLocationName = pad?.locality,
        launchpadLocationRegion = pad?.region,
        launchpadSuccessfulLanding = pad?.launchSuccesses,
    )
}
