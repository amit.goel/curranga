package com.ghn.curranga.api

import com.ghn.curranga.data.model.request.MongoosePaginate
import com.ghn.curranga.data.model.response.LaunchResponse
import com.ghn.curranga.data.model.response.LaunchpadResponse
import com.ghn.curranga.data.model.response.RocketResponseItem
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface SpacexApi {
    @POST("v4/launches/query")
    suspend fun getLaunches(
        @Body body: MongoosePaginate
    ): LaunchResponse

    @GET("v4/rockets")
    suspend fun getRockets(): List<RocketResponseItem>

    @GET("v4/launchpads?id=true")
    suspend fun getLaunchpads(): List<LaunchpadResponse>
}
