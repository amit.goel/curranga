@file:Suppress("RemoveExplicitTypeArguments")

package com.ghn.curranga.di

import com.ghn.curranga.BuildConfig
import com.ghn.curranga.utils.FlipperHelper
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.json.Json
import okhttp3.Interceptor
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Converter
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit

val networkModule = module {

    single<Interceptor>(named("HttpInterceptor")) { provideHTTPLoggingInterceptor() }

    single(named("kotlinXConverter")) { provideRetroFitConverterFactory() }

    single<Interceptor?>(named("FlipperInterceptor")) { FlipperHelper.provideNetworkFlipperPlugin() }

    single<OkHttpClient> {
        provideDefaultOkHttpClient(
            httpLoggingInterceptor = get(named("HttpInterceptor")),
            interceptor = get(named("FlipperInterceptor"))
        )
    }

    single(named("spaceXRetrofit")) {
        provideSpaceXRetrofit(
            okHttpClient = get(),
            factory = get(named("kotlinXConverter"))
        )
    }
}

private fun provideHTTPLoggingInterceptor(): HttpLoggingInterceptor =
    HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

private fun provideRetroFitConverterFactory(): Converter.Factory {
    val json = Json {
        encodeDefaults = true
        ignoreUnknownKeys = true
        isLenient = true
    }
    return json.asConverterFactory("application/json".toMediaType())
}

private fun provideDefaultOkHttpClient(
    httpLoggingInterceptor: HttpLoggingInterceptor,
    interceptor: Interceptor?
): OkHttpClient {

    val okHttpClientBuilder = OkHttpClient.Builder()
        .connectTimeout(30.toLong(), TimeUnit.SECONDS)
        .readTimeout(60.toLong(), TimeUnit.SECONDS)
        .writeTimeout(60.toLong(), TimeUnit.SECONDS)
        .callTimeout(180.toLong(), TimeUnit.SECONDS)

    interceptor?.let {
        okHttpClientBuilder.addInterceptor(it)
    }

    if (BuildConfig.DEBUG) {
        okHttpClientBuilder.addInterceptor(httpLoggingInterceptor)
    }

    return okHttpClientBuilder.build()
}

private fun provideSpaceXRetrofit(
    okHttpClient: OkHttpClient,
    factory: Converter.Factory
): Retrofit {
    return Retrofit.Builder()
        .baseUrl(BuildConfig.BASE_URL)
        .addConverterFactory(factory)
        .client(okHttpClient)
        .build()
}
