@file:Suppress("RemoveExplicitTypeArguments")

package com.ghn.curranga.di

import com.ghn.curranga.api.SpacexApi
import com.ghn.curranga.data.repository.LaunchRepository
import com.ghn.curranga.data.repository.LaunchRepositoryImpl
import com.ghn.curranga.ui.home.HomeViewModel
import com.ghn.curranga.util.extensions.createRetrofit
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit

val spaceXModule = module {
    single<SpacexApi> { provideSpacexApi(retrofit = get(named("spaceXRetrofit"))) }

    viewModel<HomeViewModel> { HomeViewModel(launchRepository = get()) }

    single<LaunchRepository> { LaunchRepositoryImpl(spacexApi = get()) }
}

private fun provideSpacexApi(retrofit: Retrofit): SpacexApi = retrofit.createRetrofit()
