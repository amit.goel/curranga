package com.ghn.curranga

import android.app.Application
import com.ghn.curranga.di.networkModule
import com.ghn.curranga.di.spaceXModule
import com.ghn.curranga.utils.FlipperHelper
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class CurrangaApp : Application() {

    override fun onCreate() {
        super.onCreate()
        val koinModules = listOf(
            networkModule, spaceXModule
        )

        startKoin {
            androidLogger()
            androidContext(this@CurrangaApp)
            modules(koinModules)
        }

        FlipperHelper.init(this)
    }
}
