package com.ghn.curranga.ui.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.ghn.curranga.R
import com.ghn.curranga.databinding.FragmentLaunchDetailsBinding
import com.ghn.curranga.ui.details.adapter.LaunchImageAdapter
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener

class LaunchDetailsFragment : Fragment() {

    private var _binding: FragmentLaunchDetailsBinding? = null

    private val binding get() = _binding!!

    private val args: LaunchDetailsFragmentArgs by navArgs()

    private val adapter = LaunchImageAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLaunchDetailsBinding.inflate(inflater, container, false)
        setUpView()
        return binding.root
    }

    private fun setUpView() = with(binding) {
        val launch = args.launchInfo
        launchName.text = launch.name

        imageResultRv.adapter = adapter
        if (launch.images?.isNotEmpty() == true) {
            adapter.submitList(launch.images)
        } else {
            launchImagesLabel.isVisible = false
            imageResultRv.isVisible = false
        }

        launchDate.text = root.context.getString(R.string.launch_date, launch.formattedDate)
        launchStatusIcon.setImageResource(launch.statusIcon)
        launchDetails.text = launch.details ?: getString(R.string.details_unavailable)
        launchRockerLabel.text = getString(R.string.launch_rocket, launch.rocketName)
        launchpadName.text = launch.launchpadName
        launchpadDetails.text = launch.launchpadDetails
        launchLocation.text = launch.location

        launch.youtubeId?.let {
            youtubePlayerView.addYouTubePlayerListener(object : AbstractYouTubePlayerListener() {
                override fun onReady(youTubePlayer: YouTubePlayer) {
                    youTubePlayer.cueVideo(it, 0f)
                }
            })
        } ?: run {
            launchVideoLabel.isVisible = false
            youtubePlayerView.isVisible = false
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.youtubePlayerView.release()
        _binding = null
    }
}
