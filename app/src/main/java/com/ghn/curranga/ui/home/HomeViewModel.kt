package com.ghn.curranga.ui.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ghn.curranga.data.repository.LaunchRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class HomeViewModel(private val launchRepository: LaunchRepository) : ViewModel() {

    private var page = 0

    private val _launchState = MutableStateFlow(LaunchState(loading = true))
    val launchState = _launchState.asStateFlow()

    init {
        getNextPage()
    }

    fun getNextPage() {
        page += 1
        viewModelScope.launch {
            val launches = launchRepository.getLaunches(page)
            _launchState.update {
                if (launches != null) {
                    it.copy(loading = false, items = it.items + launches)
                } else {
                    LaunchState(error = true)
                }
            }
        }
    }
}
