package com.ghn.curranga.ui.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.ghn.curranga.R
import com.ghn.curranga.databinding.ItemSpaceXLaunchBinding
import com.ghn.curranga.ui.home.LaunchInfo

class LaunchAdapter(
    private val clickListener: (LaunchInfo) -> Unit
) : ListAdapter<LaunchInfo, LaunchAdapter.LaunchViewHolder>(
    DiffCallback()
) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LaunchViewHolder {
        val itemBinding =
            ItemSpaceXLaunchBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return LaunchViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: LaunchViewHolder, position: Int) {
        val item = getItem(position)
        holder.itemView.setOnClickListener { clickListener(item) }
        holder.bind(item)
    }

    class LaunchViewHolder(private val binding: ItemSpaceXLaunchBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: LaunchInfo) {
            with(binding) {
                launchName.text = item.name
                patchImage.load(item.patchIcon) {
                    crossfade(true)
                    placeholder(R.drawable.ic_space_capsule)
                    listener(onError = { _, _ ->
                        patchImage.setImageResource(R.drawable.ic_space_capsule)
                    })
                }

                val statusIcon = if (item.status == true) {
                    R.drawable.ic_baseline_check_circle_outline_24
                } else {
                    R.drawable.ic_baseline_cancel_24
                }
                launchStatusIcon.setImageResource(statusIcon)
                launchDate.text = root.context.getString(R.string.launch_date, item.formattedDate)
                launchRocket.text = root.context.getString(R.string.launch_rocket, item.rocketName)
            }
        }
    }

    private class DiffCallback : DiffUtil.ItemCallback<LaunchInfo>() {
        override fun areItemsTheSame(oldItem: LaunchInfo, newItem: LaunchInfo) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: LaunchInfo, newItem: LaunchInfo) =
            oldItem == newItem
    }
}
