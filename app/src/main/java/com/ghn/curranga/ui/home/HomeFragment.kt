package com.ghn.curranga.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ghn.curranga.R
import com.ghn.curranga.databinding.FragmentHomeBinding
import com.ghn.curranga.extensions.launchWhileStarted
import com.ghn.curranga.ui.home.adapter.LaunchAdapter
import com.ghn.curranga.util.InfiniteScrollListener
import com.ghn.curranga.util.LinearSpacingDecoration
import kotlinx.coroutines.flow.collect
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : Fragment() {

    private val homeViewModel by viewModel<HomeViewModel>()
    private var _binding: FragmentHomeBinding? = null

    private val binding get() = _binding!!

    private val adapter by lazy {
        LaunchAdapter { launchItem ->
            findNavController().navigate(
                HomeFragmentDirections.homeToLaunchDetailsFragmentAction(launchItem)
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) = with(binding) {
        super.onViewCreated(view, savedInstanceState)

        val pixelSpacing = requireContext().resources.getDimensionPixelSize(R.dimen.rv_item_margin)
        spaceXRv.adapter = adapter
        spaceXRv.addItemDecoration(LinearSpacingDecoration(pixelSpacing))
        viewLifecycleOwner.launchWhileStarted {
            homeViewModel.launchState.collect {
                adapter.submitList(it.items)
            }
        }
        val endlessScrollListener = object :
            InfiniteScrollListener(spaceXRv.layoutManager as LinearLayoutManager) {
            override fun onLoadMore(page: Int, count: Int, view: RecyclerView) {
                homeViewModel.getNextPage()
            }
        }
        spaceXRv.addOnScrollListener(endlessScrollListener)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
