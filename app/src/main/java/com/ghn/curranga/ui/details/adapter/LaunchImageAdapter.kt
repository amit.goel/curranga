package com.ghn.curranga.ui.details.adapter

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.imageLoader
import coil.request.ImageRequest
import com.ghn.curranga.databinding.ItemLaunchImageBinding

class LaunchImageAdapter : ListAdapter<String, LaunchImageAdapter.LaunchViewHolder>(
    DiffCallback()
) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LaunchViewHolder {
        val itemBinding =
            ItemLaunchImageBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return LaunchViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: LaunchViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)
    }

    class LaunchViewHolder(private val binding: ItemLaunchImageBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: String) {
            with(binding) {
                val request = ImageRequest.Builder(root.context)
                    .data(item)
                    .target(
                        onSuccess = { result -> setLaunchImage(result) },
                        onError = { _ -> launchImage.isVisible = false }
                    )
                    .build()
                root.context.imageLoader.enqueue(request)
            }
        }

        private fun ItemLaunchImageBinding.setLaunchImage(result: Drawable) {
            launchImage.run {
                isVisible = true
                setImageDrawable(result)
                updateLayoutParams<ConstraintLayout.LayoutParams> {
                    dimensionRatio =
                        "${result.intrinsicWidth}:${result.intrinsicHeight}"
                }
            }
        }
    }

    private class DiffCallback : DiffUtil.ItemCallback<String>() {
        override fun areItemsTheSame(oldItem: String, newItem: String) =
            oldItem == newItem

        override fun areContentsTheSame(oldItem: String, newItem: String) =
            oldItem == newItem
    }
}
