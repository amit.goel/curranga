package com.ghn.curranga.ui.home

import android.os.Parcelable
import com.ghn.curranga.R
import kotlinx.parcelize.Parcelize
import java.text.DateFormat
import java.util.Date

@Parcelize
data class LaunchInfo(
    val id: String?,
    val name: String?,
    val status: Boolean?,
    val date: Date?,
    val details: String?,
    val images: List<String?>?,
    val patchIcon: String?,
    val youtubeId: String?,
    val rocketName: String?,
    val launchpadName: String?,
    val launchpadDetails: String?,
    val launchpadLocationName: String?,
    val launchpadLocationRegion: String?,
    val launchpadSuccessfulLanding: Int?,
) : Parcelable {
    val location: String
        get() = when {
            launchpadLocationName != null && launchpadLocationRegion != null -> {
                "$launchpadLocationName, $launchpadLocationRegion"
            }
            launchpadLocationName != null -> launchpadLocationName
            launchpadLocationRegion != null -> launchpadLocationRegion
            else -> "Unknown"
        }

    val formattedDate: String?
        get() = date?.let { DateFormat.getDateInstance(DateFormat.MEDIUM).format(it) }

    val statusIcon: Int
        get() = if (status == true) {
            R.drawable.ic_baseline_check_circle_outline_24
        } else {
            R.drawable.ic_baseline_cancel_24
        }
}

data class LaunchState(
    val loading: Boolean = false,
    val items: List<LaunchInfo> = emptyList(),
    val error: Boolean = false
)
