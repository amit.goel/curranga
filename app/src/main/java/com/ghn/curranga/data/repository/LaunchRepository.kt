package com.ghn.curranga.data.repository

import com.ghn.curranga.data.model.response.LaunchpadResponse
import com.ghn.curranga.data.model.response.RocketResponseItem
import com.ghn.curranga.ui.home.LaunchInfo

interface LaunchRepository {
    suspend fun getLaunches(page: Int): List<LaunchInfo>?

    suspend fun getRockets(): List<RocketResponseItem>

    suspend fun getLaunchpads(): List<LaunchpadResponse>
}
