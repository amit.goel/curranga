package com.ghn.curranga.data.model.response

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RocketResponseItem(
    val height: Height?,
    val diameter: Diameter?,
    val mass: Mass?,
    @SerialName("first_stage") val firstStage: FirstStage?,
    @SerialName("second_stage") val secondStage: SecondStage?,
    val engines: Engines?,
    @SerialName("landing_legs") val landingLegs: LandingLegs?,
    @SerialName("payload_weights") val payloadWeights: List<PayloadWeight?>?,
    @SerialName("flickr_images") val flickrImages: List<String?>?,
    val name: String?,
    val type: String?,
    val active: Boolean?,
    val stages: Int?,
    val boosters: Int?,
    @SerialName("cost_per_launch") val costPerLaunch: Int?,
    @SerialName("success_rate_pct") val successRatePct: Int?,
    @SerialName("first_flight") val firstFlight: String?,
    val country: String?,
    val company: String?,
    val wikipedia: String?,
    val description: String?,
    val id: String
) {
    @Serializable
    data class Height(
        val meters: Double?,
        val feet: Double?
    )

    @Serializable
    data class Diameter(
        val meters: Double?,
        val feet: Double?
    )

    @Serializable
    data class Mass(
        val kg: Int?,
        val lb: Int?
    )

    @Serializable
    data class FirstStage(
        @SerialName("thrust_sea_level") val thrustSeaLevel: ThrustSeaLevel?,
        @SerialName("thrust_vacuum") val thrustVacuum: ThrustVacuum?,
        val reusable: Boolean?,
        val engines: Int?,
        @SerialName("fuel_amount_tons") val fuelAmountTons: Double?,
        @SerialName("burn_time_sec") val burnTimeSec: Int?
    ) {
        @Serializable
        data class ThrustSeaLevel(
            val kN: Int?,
            val lbf: Int?
        )

        @Serializable
        data class ThrustVacuum(
            val kN: Int?,
            val lbf: Int?
        )
    }

    @Serializable
    data class SecondStage(
        val thrust: Thrust?,
        val payloads: Payloads?,
        val reusable: Boolean?,
        val engines: Int?,
        @SerialName("fuel_amount_tons") val fuelAmountTons: Double?,
        @SerialName("burn_time_sec") val burnTimeSec: Double?
    ) {
        @Serializable
        data class Thrust(
            val kN: Int?,
            val lbf: Int?
        )

        @Serializable
        data class Payloads(
            @SerialName("composite_fairing") val compositeFairing: CompositeFairing?,
            @SerialName("option_1") val option1: String?
        ) {
            @Serializable
            data class CompositeFairing(
                val height: Height?,
                val diameter: Diameter?
            ) {
                @Serializable
                data class Height(
                    val meters: Double?,
                    val feet: Double?
                )

                @Serializable
                data class Diameter(
                    val meters: Double?,
                    val feet: Double?
                )
            }
        }
    }

    @Serializable
    data class Engines(
        val isp: Isp?,
        @SerialName("thrust_sea_level") val thrustSeaLevel: ThrustSeaLevel?,
        @SerialName("thrust_vacuum") val thrustVacuum: ThrustVacuum?,
        val number: Int?,
        val type: String?,
        val version: String?,
        @SerialName("propellant_1") val propellant1: String?,
        @SerialName("propellant_2") val propellant2: String?,
        @SerialName("thrust_to_weight") val thrustToWeight: Double?
    ) {
        @Serializable
        data class Isp(
            @SerialName("sea_level") val seaLevel: Int?,
            val vacuum: Int?
        )

        @Serializable
        data class ThrustSeaLevel(
            val kN: Int?,
            val lbf: Int?
        )

        @Serializable
        data class ThrustVacuum(
            val kN: Int?,
            val lbf: Int?
        )
    }

    @Serializable
    data class LandingLegs(
        val number: Int?,
        val material: String?
    )

    @Serializable
    data class PayloadWeight(
        val id: String?,
        val name: String?,
        val kg: Int?,
        val lb: Int?
    )
}
