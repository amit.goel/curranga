package com.ghn.curranga.data.repository

import com.ghn.curranga.api.SpacexApi
import com.ghn.curranga.data.model.request.MongooseOptions
import com.ghn.curranga.data.model.request.MongoosePaginate
import com.ghn.curranga.data.model.response.LaunchpadResponse
import com.ghn.curranga.data.model.response.RocketResponseItem
import com.ghn.curranga.extensions.toLaunchInfo
import com.ghn.curranga.ui.home.LaunchInfo

class LaunchRepositoryImpl(private val spacexApi: SpacexApi) : LaunchRepository {

    private val rockets = mutableMapOf<String, RocketResponseItem>()
    private val launchPads = mutableMapOf<String, LaunchpadResponse>()

    // TODO: Add better error handling
    override suspend fun getLaunches(page: Int): List<LaunchInfo>? {
        try {
            runCatching {
                if (rockets.isEmpty()) {
                    getRockets().forEach { rockets[it.id] = it }
                }
            }

            runCatching {
                if (launchPads.isEmpty()) {
                    getLaunchpads().forEach { launchPads[it.id] = it }
                }
            }

            val paginate = MongoosePaginate(null, MongooseOptions(page = page))
            val launches = spacexApi.getLaunches(paginate)
            return launches.docs.map { launch ->
                val pad = launchPads[launch.launchpad.orEmpty()]
                launch.toLaunchInfo(pad, rockets[launch.rocket.orEmpty()])
            }
        } catch (e: Exception) {
            return null
        }
    }

    override suspend fun getRockets(): List<RocketResponseItem> {
        return spacexApi.getRockets()
    }

    override suspend fun getLaunchpads(): List<LaunchpadResponse> {
        return spacexApi.getLaunchpads()
    }
}
