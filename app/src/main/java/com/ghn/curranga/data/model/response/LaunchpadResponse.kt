package com.ghn.curranga.data.model.response

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class LaunchpadResponse(
    val images: Images?,
    val name: String?,
    @SerialName("full_name") val fullName: String?,
    val locality: String?,
    val region: String?,
    val latitude: Double?,
    val longitude: Double?,
    @SerialName("launch_attempts") val launchAttempts: Int?,
    @SerialName("launch_successes") val launchSuccesses: Int?,
    val rockets: List<String?>?,
    val timezone: String?,
    val launches: List<String?>?,
    val status: String?,
    val details: String?,
    val id: String
) {
    @Serializable
    data class Images(
        val large: List<String?>?
    )
}
