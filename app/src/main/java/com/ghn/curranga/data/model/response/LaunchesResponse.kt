package com.ghn.curranga.data.model.response

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class LaunchResponse(
    val docs: List<LaunchItem>,
    val totalDocs: Int,
    val offset: Int? = null,
    val limit: Int,
    val totalPages: Int,
    val page: Int,
    val pagingCounter: Int,
    val hasPrevPage: Boolean,
    val hasNextPage: Boolean,
    val prevPage: Int?,
    val nextPage: Int?,
)

@Serializable
data class LaunchItem(
    val fairings: Fairings?,
    val links: Links?,
    @SerialName("static_fire_date_utc") val staticFireDateUtc: String?,
    @SerialName("static_fire_date_unix") val staticFireDateUnix: Int?,
    val net: Boolean?,
    val window: Int?,
    val rocket: String?,
    val success: Boolean?,
    val details: String?,
    val payloads: List<String?>?,
    val launchpad: String?,
    @SerialName("flight_number") val flightNumber: Int?,
    val name: String?,
    @SerialName("date_utc") val dateUtc: String?,
    @SerialName("date_unix") val dateUnix: Int?,
    @SerialName("date_local") val dateLocal: String?,
    @SerialName("date_precision") val datePrecision: String?,
    val upcoming: Boolean?,
    val cores: List<Core?>?,
    @SerialName("auto_update") val autoUpdate: Boolean?,
    val tbd: Boolean?,
    @SerialName("launch_library_id") val launchLibraryId: String?,
    val id: String?
) {
    @Serializable
    data class Fairings(
        val reused: Boolean?,
        @SerialName("recovery_attempt") val recoveryAttempt: Boolean?,
        val recovered: Boolean?
    )

    @Serializable
    data class Links(
        val patch: Patch?,
        val flickr: Flickr?,
        val webcast: String?,
        @SerialName("youtube_id") val youtubeId: String?,
        val article: String?,
        val wikipedia: String?
    ) {
        @Serializable
        data class Patch(
            val small: String?,
            val large: String?
        )

        @Serializable
        data class Flickr(
            val small: List<String?>?,
            val original: List<String?>?
        )
    }

    @Serializable
    data class Core(
        val core: String?,
        val flight: Int?,
        val gridfins: Boolean?,
        val legs: Boolean?,
        val reused: Boolean?,
        @SerialName("landing_attempt") val landingAttempt: Boolean?,
        @SerialName("landing_success") val landingSuccess: Boolean?,
        @SerialName("landing_type") val landingType: String?,
    )
}
