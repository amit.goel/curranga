package com.ghn.curranga.data.model.request

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class MongoosePaginate(
    val query: MongooseQuery? = null,
    val options: MongooseOptions
)

@Serializable
data class MongooseQuery(
    @SerialName("\$text")
    val text: Map<String, String>
)

@Serializable
data class MongooseOptions(
    val limit: Int = 15,
    val page: Int,
//    val pagination: Boolean = false,
    val sort: Map<String, String> = mapOf("flight_number" to "desc")
)
