package com.ghn.curranga.util.extensions

import retrofit2.Retrofit

inline fun <reified T> Retrofit.createRetrofit(): T = this.create(T::class.java)
