package com.ghn.curranga.util

import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

object DateUtils {
    fun getFormattedDate(dateString: String?): Date? {
        val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault())
        return runCatching { parser.parse(dateString.orEmpty()) }.getOrNull()
    }
}
