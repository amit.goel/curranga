package com.ghn.curranga.util.extensions

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator

fun View.show(
    animationDuration: Int = 500,
    alphaValue: Float = 0f,
    scaleX: Float = 0f,
    scaleY: Float = 0f,
    block: (() -> Unit)? = null
) {
    visibility = View.VISIBLE
    alpha = alphaValue
    this.scaleX = scaleX
    this.scaleY = scaleY

    animate().scaleX(1f)
        .scaleY(1f)
        .alpha(1.0f)
        .setInterpolator(AccelerateDecelerateInterpolator())
        .setDuration(animationDuration.toLong())
        .setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                this@show.visibility = View.VISIBLE

                block?.invoke()
            }
        })
}

fun View.hide(animationDuration: Int = 500, block: (() -> Unit)? = null) {
    animate().scaleX(0f)
        .scaleY(0f)
        .alpha(0.0f)
        .setDuration(animationDuration.toLong())
        .setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                this@hide.visibility = View.GONE
                scaleX = 1f
                scaleY = 1f
                alpha = 1.0f
                block?.invoke()
            }
        })
}
