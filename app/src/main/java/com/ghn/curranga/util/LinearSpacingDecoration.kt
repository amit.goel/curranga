package com.ghn.curranga.util

import android.graphics.Rect
import android.view.View
import androidx.annotation.Px
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class LinearSpacingDecoration(@Px private val itemSpacing: Int) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        outRect.run {
            if ((parent.layoutManager as? LinearLayoutManager)?.orientation ==
                LinearLayoutManager.VERTICAL
            ) {
                left = itemSpacing
                right = itemSpacing
                bottom = itemSpacing
            } else {
                left = if (parent.getChildLayoutPosition(view) == 0) itemSpacing else 0
                right = itemSpacing
            }
        }
    }
}
