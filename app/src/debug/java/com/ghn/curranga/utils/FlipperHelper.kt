package com.ghn.curranga.utils

import android.app.Application
import com.facebook.flipper.android.AndroidFlipperClient
import com.facebook.flipper.android.utils.FlipperUtils
import com.facebook.flipper.plugins.inspector.DescriptorMapping
import com.facebook.flipper.plugins.inspector.InspectorFlipperPlugin
import com.facebook.flipper.plugins.network.FlipperOkhttpInterceptor
import com.facebook.flipper.plugins.network.NetworkFlipperPlugin
import com.facebook.soloader.SoLoader
import com.ghn.curranga.BuildConfig
import okhttp3.Interceptor

@Suppress("RedundantNullableReturnType")
object FlipperHelper {

    private val networkFlipperPlugin = NetworkFlipperPlugin()

    fun provideNetworkFlipperPlugin(): Interceptor? =
        FlipperOkhttpInterceptor(networkFlipperPlugin, true)

    fun init(application: Application) {
        SoLoader.init(application, false)
        if (BuildConfig.DEBUG && FlipperUtils.shouldEnableFlipper(application)) {
            val client = AndroidFlipperClient.getInstance(application)
            client.addPlugin(InspectorFlipperPlugin(application, DescriptorMapping.withDefaults()))
            client.addPlugin(networkFlipperPlugin)
            client.start()
        }
    }
}
