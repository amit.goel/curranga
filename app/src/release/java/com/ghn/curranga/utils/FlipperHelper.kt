package com.ghn.curranga.utils

import android.content.Context
import okhttp3.Interceptor

object FlipperHelper {
    fun getFlipperOkhttpInterceptor(): Interceptor? = null

    fun initialize(context: Context, flipperEnabled: Boolean) = Unit
}
